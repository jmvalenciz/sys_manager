#ifndef APP_HPP
#define APP_HPP

#include "GUI.hpp"

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif


class App : public wxApp{
  public:
    virtual bool OnInit();
};

#endif