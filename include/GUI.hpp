#ifndef GUI_HPP
#define GUI_HPP

#include <stdlib.h>
#include <string>
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/menu.h>
#include <wx/timer.h>

#include "sys_manager.hpp"
#include "proc.hpp"

class GUI: public wxFrame{
  wxTimer *m_timer;
  wxListCtrl* m_item_list;
  unsigned int tmp_pid;
  sys_manager* sm;

  void OnElementClick(wxListEvent& event);
  void OnPopupSelection(wxCommandEvent& event);
  void OnTimer(wxTimerEvent & event);
  DECLARE_EVENT_TABLE();

  public:
    GUI();
};

#endif