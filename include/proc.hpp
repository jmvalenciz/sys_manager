#ifndef PROC_HPP
#define PROC_HPP

#include <stdio.h>
#include <errno.h>
#include <linux/unistd.h>       
#include <linux/kernel.h>       
#include <sys/sysinfo.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <vector>

#include <string>
#include <filesystem>
#include <fstream>

class Proc{
    unsigned int pid;
    std::string path;
    std::string name;
    unsigned int mem;
    float cpu_usage_before;
    float total_hz_before;
    float cpu_pct;

    unsigned int get_number(std::string);

    float cpu_usage(std::string path);
    float total_hz();
  public:
    Proc(std::string path);
    void update_me();
    std::string get_name();
    unsigned int get_pid();
    unsigned int get_mem();
    float get_cpu();
    float get_hz();
    float get_old_cpu();
};

#endif