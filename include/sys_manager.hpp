#ifndef SYS_MANAGER_HPP
#define SYS_MANAGER_HPP

#include <list>
#include <signal.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <filesystem>
#include "proc.hpp"

namespace fs = std::filesystem;

//singleton
class sys_manager{
  private:

    static sys_manager* sm;
    std::list<Proc> proc_list;
    unsigned int max_mem;
    unsigned int available_mem;

    sys_manager();
    
    void get_mem_info();
    unsigned int get_number(std::string str);
    bool is_pid(std::string str);

  public:

    sys_manager(sys_manager & other) = delete;
    void operator=(sys_manager & other) = delete;
    static sys_manager* get_instance();

    void kill_by_pid(unsigned int pid);
    void terminate_by_pid(unsigned int pid);
    void kill_by_name(std::string name);
    void terminate_by_name(std::string name);
    void update();
    std::list<Proc> get_procs();
};


#endif