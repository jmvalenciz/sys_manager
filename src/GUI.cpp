#include "GUI.hpp"

// Tabla de manejo de eventos
BEGIN_EVENT_TABLE(GUI,wxFrame)
    EVT_LIST_ITEM_RIGHT_CLICK(0,GUI::OnElementClick)
    EVT_TIMER(1, GUI::OnTimer)
END_EVENT_TABLE()

void GUI::OnElementClick(wxListEvent& event){
    wxMenu popupMenu;
    this->tmp_pid = std::stoi((std::string)event.GetText());
    popupMenu.Append(0, 	"Terminate");
 	popupMenu.Append(1, 	"&Kill");
    popupMenu.Connect(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(GUI::OnPopupSelection), NULL, this);
    PopupMenu(&popupMenu);
}

void GUI::OnPopupSelection(wxCommandEvent& event){
    sys_manager* sm = sys_manager::get_instance();
    
    switch (event.GetId()){
    if(this->tmp_pid != 0){
        case 0:
            sm->terminate_by_pid(this->tmp_pid);
            break;
        case 1:
            sm->kill_by_pid(this->tmp_pid);
            break;
        }
    }
    this->tmp_pid = 0;
}

GUI::GUI(): wxFrame(NULL, wxID_ANY,  wxT("sys_manager"), wxPoint(50,50), wxSize(800,600)){
    m_timer = new wxTimer(this, 1);
    m_timer->Start(5000);
    wxPanel* mainPane = new wxPanel(this);
    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    this->sm = sys_manager::get_instance();
    std::string columns[] = {"PID", "Name", "CPU", "MEM"};
    sm->update();
    auto proc_list = sm->get_procs();
    this->SetIcon(wxIcon("../img/htop.ico"));
    
    m_item_list = new wxListCtrl(mainPane, 0, wxDefaultPosition, wxDefaultSize, wxLC_REPORT);
    for(int i =0;i<sizeof(columns)/sizeof(columns[0]); i++){
        wxListItem col0;
        col0.SetId(i);
        col0.SetText( _(columns[i]) );
        col0.SetWidth(columns[i]=="Name"?300:100);
        m_item_list->InsertColumn(i, col0);
    }
    
    int id =0;

    for (Proc curritem: proc_list){
        
        std::stringstream ss;
        ss << curritem.get_cpu();
        ss << "%";
        std::string s(ss.str());

        wxListItem item;
        item.SetId(id);
        item.SetText( curritem.get_name() );
        item.SetData( curritem.get_pid() );
        
        m_item_list->InsertItem( item );
        m_item_list->SetItem(id, 0, std::to_string(curritem.get_pid()));
        m_item_list->SetItem(id, 1, curritem.get_name());
        m_item_list->SetItem(id, 2, s);
        m_item_list->SetItem(id, 3, wxString::Format(wxT("%i KB"),curritem.get_mem()));

        id++;
    }
    sizer->Add(m_item_list,1, wxEXPAND | wxALL, 10);
    mainPane->SetSizer(sizer);

}

void GUI::OnTimer(wxTimerEvent & event){

    this->sm->update();
    this->m_item_list->ClearAll();
    auto proc_list = this->sm->get_procs();
    int id =0;

    std::string columns[] = {"PID", "Name", "CPU", "MEM"};
    for(int i =0;i<sizeof(columns)/sizeof(columns[0]); i++){
        wxListItem col0;
        col0.SetId(i);
        col0.SetText( _(columns[i]) );
        col0.SetWidth(columns[i]=="Name"?300:100);
        m_item_list->InsertColumn(i, col0);
    }

    for (Proc curritem: proc_list){
        
        std::stringstream ss;
        ss << curritem.get_cpu();
        ss << "%";
        std::string s(ss.str());

        wxListItem item;
        item.SetId(id);
        item.SetText( curritem.get_name() );
        item.SetData( curritem.get_pid() );
        
        m_item_list->InsertItem( item );
        m_item_list->SetItem(id, 0, std::to_string(curritem.get_pid()));
        m_item_list->SetItem(id, 1, curritem.get_name());
        m_item_list->SetItem(id, 2, s);
        m_item_list->SetItem(id, 3, wxString::Format(wxT("%i KB"),curritem.get_mem()));

        id++;
    }

    this->Update();
    this->Refresh();
}