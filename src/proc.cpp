#include "proc.hpp"

Proc::Proc(std::string path){
    std::ifstream proc_status(path + "/status");
    std::string line;

    this->path = path;

    for(int i=0; std::getline(proc_status, line); i++){
        if(i==0){
            this->name = line.substr(6);
        }
        else if(i==5){
            this->pid = stoi(line.substr(5));
        }
        else if(i==21){
            this->mem = get_number(line);
        }
    }

    this->cpu_usage_before = cpu_usage(path);
    this->total_hz_before = total_hz();
}

unsigned int Proc::get_number(std::string str){
    std::string number;
    for(char c: str){
        if(isdigit(c)){
            number += c;
        }
    }
    return atoi(number.c_str());
}

std::string Proc::get_name(){
    return this->name;
}
unsigned int Proc::get_pid(){
    return this->pid;
}
unsigned int Proc::get_mem(){
    return this->mem;
}

float Proc::cpu_usage(std::string path){
    std::ifstream inFile;
    std::string txtinfo;
    std::string* cpuArray=new std::string[55];
    int cont=0;
    path=path+"/stat";
    inFile.open(path);
    if (!inFile) {
        std::cerr << "Unable to open file " << path << std::endl;
        exit(1);   
    }
    while (inFile >> txtinfo) {
        cpuArray[cont]=txtinfo;
        cont++;
    }
    inFile.close();

    float t1 = std::stof(cpuArray[13])+std::stof(cpuArray[14]);
    return t1;
}

float Proc::total_hz(){
    std::ifstream filestat("/proc/stat");
    std::string line;
    std::getline(filestat, line);
    unsigned n1, n2, n3, n4, n5, n6, n7, n8, n9;
    float total;
    if(std::istringstream(line).ignore(3) >> n1 >> n2 >> n3 >> n4 >> n5 >> n6 >> n7 >> n8 >> n9){
        total=n1+n2+n3+n4+n5+n6+n7+n8+n9;
    }
    return total;
}

float Proc::get_cpu(){
    return this->cpu_pct;
}

float Proc::get_hz(){
    return this->total_hz_before;
}

float Proc::get_old_cpu(){
    return this->cpu_usage_before;
}

void Proc::update_me(){
    float final_usage1;
    float final_usage2;
    float final_usage3;
    //std::cout << "Updating " << this->get_name() << std::endl;
    float cpu_usage_now, total_hz_now;
    cpu_usage_now = cpu_usage(this->path);
    //std::cout << "pid path: " << this->path << std::endl;
    //std:: cout << "old cpu usage read: " << this->cpu_usage_before << std::endl;
    //std:: cout << "current cpu usage read: " << cpu_usage_now << std::endl;
    total_hz_now = total_hz();
    //std:: cout << "old total hertz: " << this->total_hz_before << std::endl;
    //std:: cout << "current total hertz: " << total_hz_now << std::endl;
    //this->cpu_pct = 100*((cpu_usage_now-this->cpu_usage_before)/(total_hz_now-this->total_hz_before));
    final_usage1=cpu_usage_now-this->cpu_usage_before;
    final_usage2=final_usage1/(total_hz_now-this->total_hz_before);
    final_usage3=final_usage2*100;
    this->cpu_pct=final_usage3;
    //std::cout << "Calculated cpu pct: " << this->cpu_pct << std::endl;
}
