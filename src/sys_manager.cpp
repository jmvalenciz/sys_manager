#include "sys_manager.hpp"

sys_manager* sys_manager::sm = nullptr;

sys_manager::sys_manager(){
    get_mem_info();
}

sys_manager* sys_manager::get_instance(){
    if(sm == nullptr){
        sm = new sys_manager;
    }
    return sm;
}

void sys_manager::kill_by_pid(unsigned int pid){
    kill(pid, SIGKILL);
}

void sys_manager::terminate_by_pid(unsigned int pid){
    kill(pid, SIGTERM);
}

void sys_manager::kill_by_name(std::string name){
    for(Proc proc : this->proc_list){
        if(proc.get_name() == name){
            kill(proc.get_pid(), SIGKILL);
        }
    }
}

void sys_manager::terminate_by_name(std::string name){
    for(Proc proc : this->proc_list){
        if(proc.get_name() == name){
            kill(proc.get_pid(), SIGTERM);
        }
    }
}

void sys_manager::update(){
    std::string path = "/proc";

    for(const auto& entry: fs::directory_iterator(path)){
        if(entry.is_directory()){
            if(is_pid(entry.path().stem().string())){
                if(get_procs().size() == 0){
                    proc_list.push_back(Proc(entry.path()));
                }
                else{
                    bool exists = false;
                    for(Proc p : this->get_procs()){
                        if(p.get_pid() == atoi(entry.path().stem().c_str())){
                            exists = true;
                            break;
                        }
                    }
                    if(!exists){
                        proc_list.push_back(Proc(entry.path()));
                    }
                }
            }
        }
    }


    for(auto& procesito:proc_list){
        procesito.update_me();
    }

    get_mem_info();
}

std::list<Proc> sys_manager::get_procs(){
    return this->proc_list;
}

bool sys_manager::is_pid(std::string str){
    for(char c: str){
        if(!isdigit(c)){
            return false;
        }
    }
    return true;
}

unsigned int sys_manager::get_number(std::string str){
    std::string number;
    for(char c: str){
        if(isdigit(c)){
            number += c;
        }
    }
    return stoi(number);
}

void sys_manager::get_mem_info(){
    std::ifstream proc_status("/proc/meminfo");
    std::string line;
    for(int i=0; std::getline(proc_status, line); i++){
        if(i==0){
            this->max_mem = get_number(line);
        }
        else if(i==2){
            this->available_mem = get_number(line);
        }
    }
}